var PlayerController = function(game, player){
	this.game = game;
	this.player = player;
	this.keys = {
		jump: game.input.keyboard.addKey(global.phaser.Keyboard.SPACEBAR),
		right: game.input.keyboard.addKey(global.phaser.Keyboard.D),
		left: game.input.keyboard.addKey(global.phaser.Keyboard.A),
		F: game.input.keyboard.addKey(global.phaser.Keyboard.F),
	}
	this.calculJumpPhysics((33*2)*4, 0.5);
	this.player.body.gravity.y = this.gravity;
	this.moveSpeed = 600;
	this.wallSlideSpeedMax = 300;
	this.jumpedLastFrame = false;
	this.stickedToWall = false;
	this.timeToStickToWall = 300;
	this.timerStickToWall = 0;
	this.wallSliding = false;
	this.wallDirX = 0;
}

PlayerController.prototype.calculJumpPhysics = function(jumpHeight, timeToJumpApex){
	this.gravity = (2*jumpHeight) / Math.pow(timeToJumpApex, 2);
	this.jumpVelocity = Math.abs(this.gravity) * timeToJumpApex;
}

PlayerController.prototype.move = function(){
	if(this.player.body.blocked.left){
		this.wallDirX = 1;
	}
	if(this.player.body.blocked.right){
		this.wallDirX = -1;
	}
	var xDirection = Math.sign(this.player.body.velocity.x);

	if(this.stickedToWall){
		if(this.timerStickToWall >= this.timeToStickToWall){
			this.stickedToWall = false;
			this.timerStickToWall = 0;
		}else{
			this.timerStickToWall += this.game.time.elapsed;
		}
	}

	//Si la touche gauche est pressée
	if(this.keys.left.isDown){

		if(this.stickedToWall){
			if(this.wallDirX == 1){
				this.player.body.velocity.x = -this.moveSpeed;
			}

		}else{
			//Si la velocité horizontal est plus grande que la vitesse du joueur
			if(Math.abs(this.player.body.velocity.x) > this.moveSpeed){
				this.player.body.velocity.x += 50 * -xDirection;
			}else{
				this.player.body.velocity.x = -this.moveSpeed;
			}
		}
	}

	//Si la touche droite est pressée
	if(this.keys.right.isDown){
		if(this.stickedToWall){
			if(this.wallDirX == -1){
				this.player.body.velocity.x = this.moveSpeed;
			}

		}else{
			//Si la velocité horizontal est plus grande que la vitesse du joueur
			if(Math.abs(this.player.body.velocity.x) > this.moveSpeed){
				this.player.body.velocity.x += 50 * -xDirection;
			}else{
				this.player.body.velocity.x = this.moveSpeed;
			}
		}
	}

	//Si les deux touche de deplacement horizontal sont pressée ou non en meme temps
	if(!this.keys.left.isDown && !this.keys.right.isDown || this.keys.left.isDown && this.keys.right.isDown){
		this.player.body.velocity.x = 0;
	}

	//Si le joueur est blocké horizontalement et qui n'est pas au sol
	if((this.player.body.blocked.left || this.player.body.blocked.right) && !this.player.body.blocked.down){
		if(!this.wallSliding){
			this.stickedToWall = true;
		}
		
		//Si le joueur tombe
		if(this.player.body.velocity.y > 0){
			this.player.body.velocity.y = this.wallSlideSpeedMax;
		}
		this.wallSliding = true;
	}else{
		if(this.stickedToWall){
			this.wallSliding = true;
		}else{
			this.wallSliding = false;
		}
	}

	//Si le touche de saut est pressée et qu'elle ne l'etait pas la frame d'avant
	if(this.keys.jump.isDown && !this.jumpedLastFrame){
		//Si le joueur est en train de wallslide
		if(this.wallSliding){
			this.player.body.velocity.x = this.wallDirX * 1000;
			this.player.body.velocity.y = -this.jumpVelocity;
			this.stickedToWall = false;
			this.timerStickToWall = 0;
		}
		//Si le joueur est au sol
		if(this.player.body.blocked.down){
			this.player.body.velocity.y = -this.jumpVelocity;
		}
		this.jumpedLastFrame = true;
	}
	//Si la touche de saut n'est pas pressée
	if(!this.keys.jump.isDown){
		this.jumpedLastFrame = false;
	}
}

PlayerController.prototype.debug = function(){
	this.game.debug.text("FPS : " + this.game.time.fps, 0, 20);
	this.game.debug.text("WallDirX : " + this.wallDirX, 0, 60);
	this.game.debug.text("wallSliding : " + this.wallSliding, 0, 80);
	this.game.debug.text("StickedToWall : " + this.stickedToWall, 0, 100);
}

module.exports = PlayerController;