var Player = function(game){
	global.phaser.Sprite.call(this, game, 100, 100, "player");
	this.scale.setTo(2);
	this.anchor.setTo(0.5, 0.5);
	game.physics.enable(this, global.phaser.Physics.ARCADE);
	game.add.existing(this);
}

Player.prototype = Object.create(global.phaser.Sprite.prototype);
Player.prototype.constructor = Player;

module.exports = Player;