window.onload = function(){
	global.phaser = Phaser;
	var game = new Phaser.Game(1280, 720, Phaser.WEBGL, "", null, false, false);

	game.state.add("Boot", require("./js/states/boot"));
	game.state.add("PreLoader", require("./js/states/preLoader"));
	game.state.add("MainMenu", require("./js/states/mainMenu"));
	game.state.add("Level1", require("./js/states/level1"));

	game.state.start("Boot");
}