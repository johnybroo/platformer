var Boot = function(game){

}

Boot.prototype = {
	init: function(){
		this.input.maxPointers = 1;
		this.time.advancedTiming = true;
		this.stage.disableVisibilityChange = true;
		this.game.renderer.renderSession.roundPixels = true;
	},
	preload: function(){
		this.load.image("preloaderBar", "./assets/images/preloaderBar.png");
	},
	create:function(){
		this.state.start("PreLoader");
	}
}

module.exports = Boot;