var Player = require("../objects/player");
var PlayerController = require("../objects/PlayerController");

var MainMenu = function(game){
}

MainMenu.prototype = {
	create: function(){
		this.playButton = this.add.button(this.game.world.centerX,this.game.world.centerY,"button",function(){this.state.start("Level1");}, this);
		this.playButton.anchor.setTo(0.5,0.5);
		var style = {
			font: "bold 32px Arial",
			fill: "#000"
		};
		this.playText = this.add.text(0, 0, "Play", style);
		this.playText.anchor.setTo(0.5, 0.5);
		this.playButton.addChild(this.playText);
	},
	update:function(){
	}
}

module.exports = MainMenu;