var Preloader = function(game){
	this.preloaderBar = null;
}

Preloader.prototype = {
	preload: function(game){
		console.log(game);
		this.preloaderBar = this.add.sprite(game.world.centerX, game.world.centerY, "preloaderBar");
		this.preloaderBar.anchor.setTo(0.5, 0.5);
		this.load.setPreloadSprite(this.preloaderBar);

		this.load.tilemap("map", "./assets/maps/Level1.json", null, global.phaser.Tilemap.TILED_JSON);
		this.load.image("tileset", "./assets/maps/01.png");
		this.load.image("player", "./assets/images/player.png");
		this.load.image("button", "./assets/images/button.png");
		this.load.image("cameraTarget", "./assets/images/cameraTarget.png");
		this.load.image("background", "./assets/images/background.jpg");
	},
	create:function(){
		this.state.start("MainMenu");
	}
}

module.exports = Preloader;