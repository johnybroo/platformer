var Player = require("../objects/player");
var PlayerController = require("../objects/PlayerController");

var Level1 = function(game){
}

Level1.prototype = {
	create: function(){
		setupPhysics.call(this);
		createMap.call(this);

		this.player = new Player(this.game);
		this.playerController = new PlayerController(this.game, this.player);

		this.cameraTarget = this.game.add.sprite(this.player.x, this.player.y, "cameraTarget");
		this.cameraTarget.anchor.set(0.5);
		this.cameraTarget.renderable = false;

		this.camera.follow(this.cameraTarget, null, 0.2, 0.2);
	},
	update:function(){
		this.physics.arcade.collide(this.player, this.collisionLayer);
		this.playerController.move();
		moveCameraTarget.call(this);
	},
	render:function(){
		this.playerController.debug();
	}
}

function setupPhysics(){
	this.physics.startSystem(global.phaser.Physics.ARCADE);
	this.physics.arcade.TILE_BIAS = 64;
}

function createMap(){
	this.map = this.add.tilemap("map");
	this.map.addTilesetImage("01", "tileset");
	this.background = this.map.createLayer("back ground");
	this.background.scale.setTo(2);
	this.foregroundLayer = this.map.createLayer("Foreground");
	this.foregroundLayer.scale.setTo(2);
	this.foregroundLayer.resizeWorld();
	this.collisionLayer = this.map.createLayer("Collision");
	this.collisionLayer.setScale(2);
	this.collisionLayer.visible = false;
	this.map.setCollisionBetween(1, 10000, true, "Collision");
}

function moveCameraTarget(){
	this.player.postUpdate();
	var playerPoint = this.player.body.center;
	var mousePoint = new global.phaser.Point(this.game.input.mousePointer.x + this.camera.x, this.game.input.mousePointer.y  + this.camera.y);
	var midPoint = new global.phaser.Point();
	midPoint.x = (playerPoint.x + mousePoint.x) / 2;
	midPoint.y = (playerPoint.y + mousePoint.y) / 2;





	this.cameraTarget.position.copyFrom(midPoint);
}

module.exports = Level1;